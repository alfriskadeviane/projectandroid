package com.example.project.Database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {DataMember.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase
{
    public abstract DataMemberDAO dao();
    private static  AppDatabase appDatabase;

    public static AppDatabase initDb(Context context)
    {
        if(appDatabase == null)
            appDatabase= Room.databaseBuilder(context, AppDatabase.class, "datamember_db").allowMainThreadQueries().build();
        return appDatabase;
    }

    public static void destroyInstance(){appDatabase=null;}
}
