package com.example.project.View.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.view.MenuItem;

import com.example.project.R;
import com.example.project.View.Fragment.InstansiFragment;
import com.example.project.View.Fragment.KeluhanFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MemberActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {
    private BottomNavigationView bottomNavigationView;
    private Fragment selectedFrag= new InstansiFragment();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member);

        bottomNavigationView= findViewById(R.id.bnav);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);

        loadFragment(selectedFrag);
    }
    private boolean loadFragment(Fragment selectedFrag){
        if (selectedFrag!=null){
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.framecontainer, selectedFrag)
                    .commit();
            return true;
        }
        return false;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()){
            case R.id.menuinstansi:
                selectedFrag= new InstansiFragment();
                loadFragment(selectedFrag);
                break;
            case R.id.menulaporan:
                selectedFrag= new KeluhanFragment();
                loadFragment(selectedFrag);
                break;
        }
        return loadFragment(selectedFrag);
    }
}
