package com.example.project.View.Fragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.project.Adapter.InstansiAdapter;
import com.example.project.Model.DaftarInstansiItem;
import com.example.project.R;
import com.example.project.View.viewmodel.InstansiViewModel;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class InstansiFragment extends Fragment {
    private InstansiAdapter instansiAdapter;
    private RecyclerView rvInstansi;
    private InstansiViewModel instansiViewModel;
    public InstansiFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_instansi, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        instansiAdapter= new InstansiAdapter(getContext());
        instansiAdapter.notifyDataSetChanged();

        rvInstansi= view.findViewById(R.id.finstansi_rv);
        rvInstansi.setLayoutManager(new GridLayoutManager(getContext(),1));

        instansiViewModel= new ViewModelProvider(this).get(InstansiViewModel.class);
        instansiViewModel.setInstansi();
        instansiViewModel.getInstansi().observe(this, getInstansi_);

        rvInstansi.setAdapter(instansiAdapter);
    }
    private Observer<ArrayList<DaftarInstansiItem>> getInstansi_ = new Observer<ArrayList<DaftarInstansiItem>>() {
        @Override
        public void onChanged(ArrayList<DaftarInstansiItem> daftarInstansiItems) {
            if (daftarInstansiItems != null) {
                instansiAdapter.setData(daftarInstansiItems);
            }
        }
    };
}
