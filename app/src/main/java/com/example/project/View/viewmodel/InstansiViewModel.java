package com.example.project.View.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.project.Model.DaftarInstansiItem;
import com.example.project.Model.InstansiResponse;
import com.example.project.Service.ApiMain;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InstansiViewModel extends ViewModel {
    private ApiMain apiMain;
    private MutableLiveData<ArrayList<DaftarInstansiItem>> listInstansi= new MutableLiveData<>();
    public void setInstansi(){
        if(this.apiMain== null){
            apiMain= new ApiMain();
        }
        apiMain.getApiInstansi().getInstansi().enqueue(new Callback<InstansiResponse>() {
            @Override
            public void onResponse(Call<InstansiResponse> call, Response<InstansiResponse> response) {
                InstansiResponse instansiResponse= response.body();
                if (instansiResponse != null && instansiResponse.getDaftarInstansi() != null){
                    ArrayList<DaftarInstansiItem> daftarInstansiItems= instansiResponse.getDaftarInstansi();
                    listInstansi.postValue(daftarInstansiItems);
                }
            }

            @Override
            public void onFailure(Call<InstansiResponse> call, Throwable t) {

            }
        });
    }
    public LiveData<ArrayList<DaftarInstansiItem>> getInstansi(){
        return listInstansi;
    }
}
