package com.example.project.Service;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiMain {
    private Retrofit retrofit;
    public InstansiRepository getApiInstansi(){
        String BASE_URL= "http://dev.farizdotid.com/api/instansi/daftar_instansi/";
        if (retrofit == null){
            retrofit = new Retrofit
                    .Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit.create(InstansiRepository.class);
    }
}
