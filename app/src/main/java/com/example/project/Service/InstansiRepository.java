package com.example.project.Service;

import com.example.project.Model.InstansiResponse;

import retrofit2.Call;
import retrofit2.http.GET;

public interface InstansiRepository {
    @GET("kota%20jakarta%20utara")
    Call<InstansiResponse> getInstansi();
}