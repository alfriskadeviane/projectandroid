package com.example.project.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.project.Model.DaftarInstansiItem;
import com.example.project.R;

import java.util.ArrayList;

public class InstansiAdapter extends RecyclerView.Adapter<InstansiAdapter.ViewHolder> {
    private ArrayList<DaftarInstansiItem> daftarInstansiItems= new ArrayList<>();
    private Context context;

    public InstansiAdapter(Context context) {
        this.context= context;
    }

    public void setData(ArrayList<DaftarInstansiItem> items){
        daftarInstansiItems.clear();
        daftarInstansiItems.addAll(items);
        notifyDataSetChanged();
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View  view= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tv_nama_instansi.setText(daftarInstansiItems.get(position).getNamaInstansi());
        holder.tv_no.setText(daftarInstansiItems.get(position).getNomorInstansi());
        holder.tv_alamat.setText(daftarInstansiItems.get(position).getAlamatInstansi());
    }

    @Override
    public int getItemCount() {
        return daftarInstansiItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_nama_instansi, tv_no, tv_alamat;
        CardView cvItem;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            cvItem= itemView.findViewById(R.id.item_list_cv);
            tv_nama_instansi=itemView.findViewById(R.id.tv_nama_instansi);
            tv_no=itemView.findViewById(R.id.tv_no);
            tv_alamat=itemView.findViewById(R.id.tv_alamat);
        }
    }
}
